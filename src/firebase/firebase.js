import firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBoANZ-4hJigGTfdpsxjYNKEbYMMfxRYzY",
  authDomain: "traque-insecte-simplon.firebaseapp.com",
  projectId: "traque-insecte-simplon",
  storageBucket: "traque-insecte-simplon.appspot.com",
  messagingSenderId: "201308407775",
  appId: "1:201308407775:web:2be06d64beb7a14fa6266a"
};
firebase.initializeApp(firebaseConfig);

/**
 * Base de données Firestore
 */
const db = firebase.firestore();

/**
 * Service d'authentification
 */
const auth = firebase.auth();

/**
 * Espace de rangement
 * https://firebase.google.com/docs/storage/web/upload-files
 */
const storage = firebase.storage();

export { firebase, db, auth, storage };
